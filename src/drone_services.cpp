
#include <tf/tf.h>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <cmath>
#include <gazebo_msgs/SpawnModel.h>
#include <gazebo_msgs/ApplyBodyWrench.h>
#include <gazebo_msgs/GetLinkProperties.h>
#include <gazebo_msgs/SetLinkProperties.h>
#include <ros_project/attach_ball.h>
#include <ros_project/launch_ball.h>
#include <gazebo_ros_link_attacher/Attach.h>
#include <string>

std::string quidditch_sdf = "<?xml version='1.0'?><sdf version='1.6'><model name='quidditch_ball'>      <link name='link'><gravity>True</gravity><inertial>  <mass>0.1</mass>  <inertia>    <ixx>4.15e-6</ixx>    <ixy>0</ixy>    <ixz>0</ixz>    <iyy>2.407e-6</iyy>    <iyz>0</iyz>    <izz>2.407e-6</izz>  </inertia>  <pose frame=''>0 0 0 0 -0 0</pose></inertial><collision name='collision'>  <geometry>    <sphere>      <radius>0.1</radius>    </sphere>  </geometry>  <max_contacts>10</max_contacts>  <surface>    <contact>      <ode/>    </contact>    <bounce/>    <friction>      <torsional>        <ode/>      </torsional>      <ode/>    </friction>  </surface></collision><visual name='visual'>  <geometry>    <sphere>      <radius>0.1</radius>    </sphere>  </geometry>  <material>    <script>      <name>Gazebo/Grey</name>      <uri>file://media/materials/scripts/gazebo.material</uri>    </script>    <ambient>1 1 0 1</ambient>    <diffuse>0.7 0.7 0.7 1</diffuse>    <specular>0.01 0.01 0.01 1</specular>    <emissive>0 0 0 1</emissive>  </material></visual><self_collide>0</self_collide><enable_wind>0</enable_wind><kinematic>0</kinematic>      </link>    </model></sdf>";

ros::ServiceClient spawnService;
ros::ServiceClient attachService;
ros::ServiceClient detachService;
ros::ServiceClient forceService;
ros::ServiceClient getLinkService;
ros::ServiceClient setLinkService;

volatile float z_rotation = 0;

bool attach_ball(ros_project::attach_ball::Request &req, ros_project::attach_ball::Response &res) {
	gazebo_msgs::SpawnModel srv;
	gazebo_ros_link_attacher::Attach srv2;
	srv.request.model_name = "quidditch_ball";
	srv.request.model_xml = quidditch_sdf;
	srv.request.robot_namespace = "da_ball";
	srv.request.initial_pose.position.x = 0.3;
	srv.request.initial_pose.position.y = 0;
	srv.request.initial_pose.position.z = 0.25;
	srv.request.reference_frame = "iris_0";
	srv2.request.model_name_1 = "quidditch_ball";
	srv2.request.link_name_1 = "quidditch_ball::link";
	srv2.request.model_name_2 = "iris_0";
	srv2.request.link_name_2 = "iris_0::base_link";
	res.status = ((spawnService.call(srv) && attachService.call(srv2)) ? 120 : 0);
	return true;
}

bool launch_ball(ros_project::launch_ball::Request &req, ros_project::launch_ball::Response &res) {
	gazebo_ros_link_attacher::Attach srv;
	gazebo_msgs::GetLinkProperties srv2;
	gazebo_msgs::SetLinkProperties srv3;
	gazebo_msgs::ApplyBodyWrench srv4;
	srv.request.model_name_1 = "quidditch_ball";
	srv.request.link_name_1 = "quidditch_ball::link";
	srv.request.model_name_2 = "iris_0";
	srv.request.link_name_2 = "iris_0::base_link";
	srv2.request.link_name = "quidditch_ball::link";
	detachService.call(srv);
	getLinkService.call(srv2);
	srv3.request.link_name = "quidditch_ball::link";
	srv3.request.com = srv2.response.com;
	srv3.request.gravity_mode = true;
	srv3.request.mass = srv2.response.mass;
	srv3.request.ixx = srv2.response.ixx;
	srv3.request.ixy = srv2.response.ixy;
	srv3.request.ixz = srv2.response.ixz;
	srv3.request.iyy = srv2.response.iyy;
	srv3.request.iyz = srv2.response.iyz;
	srv3.request.izz = srv2.response.izz;
	srv4.request.body_name = "quidditch_ball::link";
	srv4.request.reference_point.x = 0.0;
	srv4.request.reference_point.y = 0.0;
	srv4.request.reference_point.z = 0.0;
	srv4.request.wrench.force.x = (7.5 * cos(z_rotation));
	srv4.request.wrench.force.y = (7.5 * sin(z_rotation));
	srv4.request.wrench.force.z = 5.75;
	srv4.request.wrench.torque.x = 0.0;
	srv4.request.wrench.torque.y = 0.0;
	srv4.request.wrench.torque.z = 0.0;
	srv4.request.duration.sec = 0;
	srv4.request.duration.nsec = 100000000;
	setLinkService.call(srv3);
	forceService.call(srv4);
	res.status = 120;
	return true;
}

void position_cb(const geometry_msgs::PoseStamped::ConstPtr& msg) {
	geometry_msgs::PoseStamped umsg = *msg;
	tf::Quaternion q(umsg.pose.orientation.x, umsg.pose.orientation.y, umsg.pose.orientation.z, umsg.pose.orientation.w);
	tf::Matrix3x3 m(q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw);
	z_rotation = yaw;
}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "drone_services");
    ros::NodeHandle nh;


    ros::ServiceServer service1 = nh.advertiseService("attach_ball", attach_ball);
    ros::ServiceServer service2 = nh.advertiseService("launch_ball", launch_ball);

    spawnService = nh.serviceClient<gazebo_msgs::SpawnModel>("/gazebo/spawn_sdf_model");
    attachService = nh.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/attach");
    detachService = nh.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/detach");
    forceService = nh.serviceClient<gazebo_msgs::ApplyBodyWrench>("/gazebo/apply_body_wrench");
    getLinkService = nh.serviceClient<gazebo_msgs::GetLinkProperties>("/gazebo/get_link_properties");
    setLinkService = nh.serviceClient<gazebo_msgs::SetLinkProperties>("/gazebo/set_link_properties");

    ros::Subscriber position_sub = nh.subscribe<geometry_msgs::PoseStamped>
	    ("/chaser/mavros/local_position/pose",10,position_cb);

    ros::spin();	

    return 0;
}
