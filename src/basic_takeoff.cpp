/**
 * @file offb_node.cpp
 * @brief Offboard control example node, written with MAVROS version 0.19.x, PX4 Pro Flight
 * Stack and tested in Gazebo SITL
 */

#include <tf/tf.h>
#include <ros/ros.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <ros_project/launch_ball.h>
#include <ros_project/attach_ball.h>
#include <cmath>
#include <ros_project/ballLocation.h>
#include <ros_project/ballDebug.h>
#include <ros_project/debugTimes.h>
#include <stdlib.h>
#include <gazebo_msgs/DeleteModel.h>

mavros_msgs::State current_state;

volatile int ball_ready = 1;
volatile int ball_launch_state = 1;

volatile float pixel_states[][3] {
	{0,0,0},
	{0,0,0},
};

ros::Time when_reported[] {
	ros::Time(0,0),
	ros::Time(0,0),
};


void state_cb(const mavros_msgs::State::ConstPtr& msg){
    current_state = *msg;
}

volatile float cur_x = 0;
volatile float cur_y = 0;
volatile float cur_z = 0;
volatile float cur_tilt = 0;
volatile float cur_z_tilt = 0;

//Switch to next target position if we are already at the current one.
void position_cb(const geometry_msgs::PoseStamped::ConstPtr& msg) {
	geometry_msgs::PoseStamped umsg = *msg;
	cur_x = umsg.pose.position.x;
	cur_y = umsg.pose.position.y;
	cur_z = umsg.pose.position.z;
	cur_tilt = fabs(umsg.pose.orientation.x) + fabs(umsg.pose.orientation.y);
	tf::Quaternion q(umsg.pose.orientation.x, umsg.pose.orientation.y, umsg.pose.orientation.z, umsg.pose.orientation.w);
	tf::Matrix3x3 m(q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw);
	cur_z_tilt = yaw;
}

int is_ball_valid(int ball) {
	return ((ros::Time::now() - when_reported[ball]) < ros::Duration(0.1));
}


// 0 - Red
// 1 - Blue
// 2 - Green
// 3 - Purple
void ballLocation_cb(const ros_project::ballLocation::ConstPtr& msg, int ball) {
	ros_project::ballLocation umsg = *msg;
	pixel_states[ball][0] = umsg.x;
	pixel_states[ball][1] = umsg.y;
	pixel_states[ball][2] = umsg.radius;
	when_reported[ball] = ros::Time::now();
}

void redballLocation_cb(const ros_project::ballLocation::ConstPtr& msg) {
	ballLocation_cb(msg, 0);
}

void blueballLocation_cb(const ros_project::ballLocation::ConstPtr& msg) {
	ballLocation_cb(msg, 1);
}


void attach_ball(ros::NodeHandle nh) {
	ros::ServiceClient attaching_client = nh.serviceClient<ros_project::attach_ball>("/attach_ball");
	ros_project::attach_ball srv;
	attaching_client.call(srv);
	ball_ready = 1;
}

void launch_ball(ros::NodeHandle nh) {
	ros::ServiceClient launching_client = nh.serviceClient<ros_project::launch_ball>("/launch_ball");
	ros_project::launch_ball srv;
	launching_client.call(srv);
	ball_ready = 0;
}

void delete_ball(ros::NodeHandle nh) {
	ros::ServiceClient deleting_client = nh.serviceClient<gazebo_msgs::DeleteModel>("/gazebo/delete_model");
	gazebo_msgs::DeleteModel srv;
	srv.request.model_name = "quidditch_ball";
	deleting_client.call(srv);
}


double rand2() {
	return ((double)rand()) / (double)RAND_MAX;
}

double rotation_rate = 0.01;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "offb_node");
    ros::NodeHandle nh;

    ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State>
            ("/chaser/mavros/state", 10, state_cb);

    ros::Subscriber position_sub = nh.subscribe<geometry_msgs::PoseStamped>
	    ("/chaser/mavros/local_position/pose",10,position_cb);
    ros::Publisher local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>
            ("/chaser/mavros/setpoint_position/local", 10);
    ros::Subscriber redballLocation_sub = nh.subscribe<ros_project::ballLocation>("/redBallDetector/ballLocation",10,redballLocation_cb);
    ros::Subscriber blueballLocation_sub = nh.subscribe<ros_project::ballLocation>("/blueBallDetector/ballLocation",10,blueballLocation_cb);
    ros::ServiceClient arming_client = nh.serviceClient<mavros_msgs::CommandBool>
            ("/chaser/mavros/cmd/arming");
    ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>
            ("/chaser/mavros/set_mode");

    //the setpoint publishing rate MUST be faster than 2Hz
    ros::Rate rate(20.0);

    geometry_msgs::PoseStamped pose;

    pose.pose.position.x = 0;
    pose.pose.position.y = 0;
    pose.pose.position.z = 6;
    pose.pose.orientation.z = 0;
    pose.pose.orientation.w = 1;

    // wait for FCU connection
    while(ros::ok() && !current_state.connected){
        ros::spinOnce();
        rate.sleep();
    }


    //send a few setpoints before starting
    for(int i = 100; ros::ok() && i > 0; --i){
        local_pos_pub.publish((geometry_msgs::PoseStamped)pose);
        ros::spinOnce();
        rate.sleep();
    }

    mavros_msgs::SetMode offb_set_mode;
    offb_set_mode.request.custom_mode = "OFFBOARD";

    mavros_msgs::CommandBool arm_cmd;
    arm_cmd.request.value = true;

    ros::Time last_request = ros::Time::now();

    double orientation = 0;
    bool ready = false;
    attach_ball(nh);
    while(ros::ok()){
        if( current_state.mode != "OFFBOARD" &&
            (ros::Time::now() - last_request > ros::Duration(5.0))){
            if( set_mode_client.call(offb_set_mode) &&
                offb_set_mode.response.mode_sent){
                ROS_INFO("Offboard enabled");
            }
            last_request = ros::Time::now();
        } else {
            if( !current_state.armed &&
                (ros::Time::now() - last_request > ros::Duration(5.0))){
                if( arming_client.call(arm_cmd) &&
                    arm_cmd.response.success){
                    ROS_INFO("Vehicle armed");
		    ready = true;
                }
                last_request = ros::Time::now();
            }
        }
	if (ready && ball_launch_state) {
		if (is_ball_valid(0) && is_ball_valid(1)) {
			float y_delta = pixel_states[1][1] - pixel_states[0][1];
			float computed_distance = (70.0 / y_delta) * 8.0;
			float averaged_x = (pixel_states[1][0] + pixel_states[0][0]) / 2.0;
			ROS_INFO("Distance: %lf %lf %lf\n", computed_distance, (double)pixel_states[1][1], (double)pixel_states[0][1]);
			if (averaged_x < 0.0) {
				orientation += rotation_rate;
				if (orientation > 3.14) {
					orientation = -3.14;
				}
			}
			else {
				orientation -= rotation_rate;
				if (orientation < -3.14) {
					orientation = 3.14;
				}
			}
			if (fabs(averaged_x) < 5.0) {
				if ((cur_tilt < 0.2) && (fabs(computed_distance - 8.0) > 0.5)) {
					float computed_distance_delta = computed_distance - 8.0;
					float computed_x_delta = computed_distance_delta * cos(cur_z_tilt);	
					float computed_y_delta = computed_distance_delta * sin(cur_z_tilt);
					ROS_INFO("Ready to close in %lf %lf %lf %lf %lf %lf!", cur_z_tilt, cos(cur_z_tilt), sin(cur_z_tilt), computed_distance_delta, computed_x_delta, computed_y_delta);
					pose.pose.position.x = cur_x + computed_x_delta;
					pose.pose.position.y = cur_y + computed_y_delta;
				}
				else if ((cur_tilt < 0.05)) {
					launch_ball(nh);
					pose.pose.position.x = (15.0 * rand2()) - 7.5;
					pose.pose.position.y = (15.0 * rand2()) - 7.5;
					pose.pose.position.z = 0;
					orientation = (6.28 * rand2()) - 3.14;
					ball_launch_state = 0;					
				};
			}	
		} 
		else {
			orientation += rotation_rate;
			if (orientation > 3.14) {
				orientation = -3.14;
			}
		};
	}
	else {
		if ((ball_launch_state == 0) && (cur_z < 0.5)) {
			delete_ball(nh);
			attach_ball(nh);
			pose.pose.position.z = 6;
			ball_launch_state = 1;
		}
	}
	tf::Quaternion q(0,0, orientation);
	pose.pose.orientation.x = q[0];
	pose.pose.orientation.y = q[1];
	pose.pose.orientation.z = q[2];
	pose.pose.orientation.w = q[3];
	local_pos_pub.publish((geometry_msgs::PoseStamped)pose);
	
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
