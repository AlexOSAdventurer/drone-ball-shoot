#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <gazebo_msgs/ModelStates.h>
#include <string>


geometry_msgs::Pose pose;

ros::Publisher pub;

void update_position(const gazebo_msgs::ModelStates::ConstPtr& states) {
	gazebo_msgs::ModelStates mstates = *states;
	for (int i = 0; i < mstates.name.size(); i++) {
		if ((mstates.name[i]).compare("quidditch_ball") == 0) {
			pose = mstates.pose[i];
			pub.publish(pose);
		}
	}
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ball_position");
    ros::NodeHandle nh;


    ros::Subscriber getModelState = nh.subscribe("/gazebo/model_states", 20, update_position);
    pub = nh.advertise<geometry_msgs::Pose>("/ballPosition", 20);


    //the setpoint publishing rate MUST be faster than 2Hz
    ros::Rate rate(20.0);

    ros::spin();

    return 0;
}

